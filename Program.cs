﻿using System;

namespace comp5002_9983505_assessment01
{
    class Program
    {
        static void Main(string[] args)
        {

/*
            Pseudo Code
            Start program
            Declare variables
            Welcome user
            Ask user for their name
            Ask user to enter first number
            Ask user if they want to add another number
	            If yes
		            Ask user for 2nd number
		            Add first number and second number
		            Display total
	            If no
		            Add number to total
		            Display total
            Add GST to total
            Display total incl GST
            Thank user
            Stop program
 */

            //Declare variables
            double num1 = 0;
            double num2 = 0;
            double itemTotal = 0;
            double total = 0;
            double gst = 0.15;
            double totalGst = 0;
            var name = "";
            var decision = "";

            //Clear Console
            Console.Clear();

            //Welcome User
            Console.WriteLine("|------------------------------------|");
            Console.WriteLine("|------------------------------------|");
            Console.WriteLine("|    Welcome to my Kadin Leeming     |");
            Console.WriteLine("|------------------------------------|");
            Console.WriteLine("|       Please enter your name       |");
            Console.WriteLine("|------------------------------------|");
            Console.WriteLine();//Create spacing

            //Ask user for their name
            name = (Console.ReadLine());

            //Ask user for their first number
            Console.WriteLine("|--------------------------------------|");
            Console.WriteLine("|--------------------------------------|");
            Console.WriteLine($"|             Hello {name}             |");
            Console.WriteLine("|  Enter the price of your first item  |");
            Console.WriteLine("|--------------------------------------|");
            Console.WriteLine();//Create spacing

            num1 = double.Parse(Console.ReadLine());

            //Ask user if they want to add a second number
            Console.WriteLine("|----------------------------------------|");
            Console.WriteLine("|----------------------------------------|");
            Console.WriteLine("|  Would you like to add a second item?  |");
            Console.WriteLine("|----------------------------------------|");
            Console.WriteLine("|          'Y' = Yes / 'N' = No          |");
            Console.WriteLine("|----------------------------------------|");
            Console.WriteLine();//Create spacing

            decision = Console.ReadLine();

            //If yes
            if (decision == "Y")
            {
                //Ask user for their second number
                Console.WriteLine("|----------------------------------------|");
                Console.WriteLine("|----------------------------------------|");
                Console.WriteLine("|                OK sure.                |");
                Console.WriteLine("|  Enter the price of your second item.  |");
                Console.WriteLine("|----------------------------------------|");
                Console.WriteLine();//Create spacing
                
                num2 = double.Parse(Console.ReadLine());

                //Add first number to second number
                itemTotal = num1+num2;

                //Display total
                Console.WriteLine("|-------------------------|");
                Console.WriteLine("|---------Reciept---------|");
                Console.WriteLine("|-------------------------|");
                Console.WriteLine($"|.First Item.......{num1:C2}.|");//":C2" Displays number as currency
                Console.WriteLine($"|.Second Item......{num2:C2}.|");
                Console.WriteLine("|                         |");
                Console.WriteLine($"|.Total Excl GST...{itemTotal:C2}.|");
                Console.WriteLine("|                         |");
            }

            //If no
            else if (decision == "N")
            {
                //Add number to total
                itemTotal = num1;

                //Display total
                Console.WriteLine("|-------------------------|");
                Console.WriteLine("|---------Reciept---------|");
                Console.WriteLine("|-------------------------|");
                Console.WriteLine($"|.First Item.......{num1:C2}.|");
                Console.WriteLine("|                         |");
                Console.WriteLine($"|.Total Excl GST...{itemTotal:C2}.|");
                Console.WriteLine("|                         |");
            }
            
            //Calculate GST
            totalGst = itemTotal*gst;
            //Add GST to total
            total = totalGst+itemTotal;

            //Display total incl GST and thank user
            Console.WriteLine($"|.GST...............{totalGst:C2}.|");
            Console.WriteLine($"|.Total............{total:C2}.|");
            Console.WriteLine("|-------------------------|");
            Console.WriteLine("| Thank you for shopping  |");
            Console.WriteLine($"|       with us {name}     |");
            Console.WriteLine("|    Please come again!   |");
            Console.WriteLine("|-------------------------|");
            Console.WriteLine("|-------------------------|");
            Console.WriteLine();//Create spacing
            Console.WriteLine("Press <ENTER> to close program");
            Console.WriteLine();//Create spacing
            Console.ReadKey();
        }
    }
}